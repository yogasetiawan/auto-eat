const express = require('express')
const CryptoJS = require("crypto-js");
const {getLocalCookie} = require("./utils");
const {getLocalToken} = require("./utils");
const {getLocalProfile} = require("./utils");
const app = express()
const PORT = process.env.PORT || 4000
app.get('/', async (req, res) => {
    const profile = await getLocalProfile()
    const token = await getLocalToken()
    const cookie = await getLocalCookie()
    profile["csrf"] = CryptoJS.MD5(token).toString();
    profile["cookie"] = CryptoJS.MD5(cookie).toString();
    res.send(profile)
});
app.listen(PORT, () => console.log(`app listening on port ${PORT}`))
