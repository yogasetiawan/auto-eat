const axios = require("axios");
const {LocalStorage} = require("node-localstorage");
const localStorage = new LocalStorage('./localstorage');

exports.getLocalCookie = () => {
    let cookie = localStorage.getItem("COOKIE")
    if(cookie){
        return JSON.parse(cookie)
    }else{
        return []
    }

}

exports.setLocalCookie = (cookie) => {
    localStorage.setItem("COOKIE", JSON.stringify(cookie))
}

exports.getLocalToken = () => localStorage.getItem("TOKEN")

exports.setLocalToken = (token) => localStorage.setItem("TOKEN", token)

exports.getLocalProfile = () => {
    const profile = JSON.parse(localStorage.getItem("PROFILE"))
    if(profile){
        return JSON.parse(profile)
    }else{
        return {}
    }

}

exports.setLocalProfile = (profile) => localStorage.setItem("PROFILE", JSON.stringify(profile))

exports.getHeader = (cookie) => {
    return {
        'authority': 'www.erepublik.com',
        'upgrade-insecure-requests': '1',
        'origin': 'https://www.erepublik.com',
        'content-type': 'application/x-www-form-urlencoded',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'sec-fetch-site': 'same-origin',
        'Cookie': cookie
    }
}

exports.getDataUser = async (cookie) => {
    return new Promise((resolve, reject) => {
        axios.get('https://www.erepublik.com/en/main/mobile-login', {
            headers: this.getHeader(cookie),
        })
            .then(function ({data}) {
                resolve(data)
            })
            .catch(function (error) {
                reject(error)
            });
    })
}
