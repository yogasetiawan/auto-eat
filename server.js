require('dotenv').config()
const qs = require('qs')
const axios = require('axios');
const {EMAIL, PASSWORD} = process.env
const cron = require('node-cron');

const {
    setLocalCookie,
    setLocalToken,
    getDataUser,
    getHeader,
    getLocalToken,
    getLocalCookie,
    setLocalProfile
} = require("./utils");


let cookie = getLocalCookie()

const requestEat = async () => {
    return new Promise(async (resolve, reject) => {
        const postData = `_token=${getLocalToken()}&buttonColor=blue`;
        const config = {
            method: 'post',
            url: 'https://www.erepublik.com/en/main/eat',
            headers: getHeader(getLocalCookie()),
            data: postData,
        };

        await axios(config)
            .then(function ({data}) {
                const {health} = data
                if (health) {
                    console.log(health)
                    resolve(data)
                } else {
                    reject(data)
                }
            })
            .catch(function (error) {
                reject(error)
            });
    })
}

const getCookie = async () => {
    return new Promise((resolve, reject) => {
        axios.get('https://www.erepublik.com/en/main/mobile-login', {
            withCredentials: true
        })
            .then(function (data) {
                resolve(data.headers['set-cookie'])
            })
            .catch(function (error) {
                reject(error)
            });
    })
}

const login = (cookie) => {
    return new Promise((resolve, reject) => {
        let data = qs.stringify({
            'citizen_email': EMAIL,
            'citizen_password': PASSWORD,
            'remember': 'on'
        });

        let config = {
            method: 'post',
            url: 'https://www.erepublik.com/en/login',
            headers: getHeader(cookie),
            data: data
        };
        axios(config)
            .then(function ({data}) {
                resolve(cookie)
            })
            .catch(function (error) {
                reject(error)
            });

    })
}

const checkCookieLogin = async (local = true) => {
    return new Promise(async (resolve, reject) => {
        let result = null
        if (local) {
            await getDataUser(getLocalCookie())
                .then((data) => {
                    return new Promise((resolve, reject) => {
                        const {csrf, email} = data
                        if (email) {
                            setLocalToken(csrf)
                            result = data
                            resolve()
                        } else {
                            reject()
                        }
                    })
                })
                .then(() => {
                    resolve(result)
                })
                .catch(() => {
                    checkCookieLogin(false)
                })
        } else {
            await getCookie()
                .then(dataCookie => {
                    return login(dataCookie)
                })
                .then(dataCookie => {
                    cookie = dataCookie
                    return getDataUser(dataCookie)
                })
                .then((data) => {
                    return new Promise((resolve2, reject2) => {
                        const {csrf, email} = data
                        if (email) {
                            setLocalToken(csrf)
                            result = data
                            resolve2()
                        } else {
                            reject2()
                        }
                    })
                })
                .then(() => {
                    setLocalCookie(cookie)
                    resolve(result)
                })
                .catch(() => {
                    reject()
                })
        }
    })
}

checkCookieLogin(getLocalToken() != null)
    .then(data => {
        const {currentEnergy, energyPool} = data
        console.log(currentEnergy, energyPool)
        setLocalProfile(data)
        if (currentEnergy < energyPool) {
            requestEat()
        }
    })
    .catch(e => {
        console.log(e)
    })

const task = cron.schedule('*/10 * * * *', async () => {
    setTimeout(function () {
        checkCookieLogin(getLocalToken() != null)
            .then(data => {
                const {currentEnergy, energyPool} = data
                setLocalProfile(data)
                if (currentEnergy < energyPool) {
                    requestEat()
                }
            })
            .catch(e => {
                console.log(e)
            })
    }, Math.floor(Math.random() * 100000));
}, null);
